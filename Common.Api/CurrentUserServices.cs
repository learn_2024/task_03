﻿using Common.App.Abstractions;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Common.Api
{
    public class CurrentUserServices : ICurrentUserServices
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserServices(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public string CurrentUserId => _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)!.Value;
        public string[] CurrentUserRoles => _httpContextAccessor.HttpContext.User.Claims.Where(c=>c.Type == ClaimTypes.Role).Select(c=>c.Value).ToArray();
        public bool IsAdmin => _httpContextAccessor.HttpContext.User.Claims.Count(c => c.Type == ClaimTypes.Role && c.Value == "Admin") > 0;
    }
}
