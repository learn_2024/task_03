using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Todo.App.Command.AddTodoTaskForAnyOwner;
using Todo.App.Command.AddTodoTaskForMe;
using Todo.App.Command.DeleteTodoTask;
using Todo.App.Command.EditTodoTask;
using Todo.App.Command.SetIsDoneField;
using Todo.App.Query.GetList;
using Todo.App.Query.GetTodoListTotalCount;
using Todo.App.Query.GetTodoTaskByID;


namespace Todo.Api
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TodoTaskController : Controller
    {
        [HttpGet("todos")]
        public async Task<IActionResult> Todos([FromQuery]GetTodoListQuery query, IMediator mediator,CancellationToken cancellationToken = default)
        {
            var list = await mediator.Send(query, cancellationToken);
            int qt = await mediator.Send(new GetTodoListTotalCountQuery() { TaskName = query.TaskName, OwnerId = query.OwnerId }, cancellationToken);
            Response.Headers.Append("x-Total-Count", qt.ToString());
            return Ok(list);
        }

        [HttpGet]
        [Route("todos/{id}")]
        public async Task<IActionResult> TodoTaskById(long id, IMediator mediator, CancellationToken cancelationToken)
        {
            var item = await mediator.Send(new GetTodoTaskByIDCommand() { Id = id }, cancelationToken);
            return Ok(item);
        }

        [HttpGet]
        [Route("todos/{id}/IsDone")]
        public async Task<IActionResult> TodosIsDoneById(long id, IMediator mediator, CancellationToken cancellationToken)
        {
            var item = await mediator.Send(new GetTodoTaskByIDCommand() { Id = id }, cancellationToken);
            return Ok(new { Id = item.Id, IsDone = item.IsDone });
        }

        [HttpPost]
        [Route("/AddAnyUserTodo")]
        public async Task<IActionResult> AddAnyUserTodo([FromBody] AddTodoTaskForAnyOwnerCommand todo, 
            IMediator mediator, CancellationToken cancellationToken)
        {
            var item = await mediator.Send(todo, cancellationToken);
            return Created($"/todos/{item.Id}", item);
        }

        [HttpPost]
        [Route("/AddOwnTodo")]
        public async Task<IActionResult> AddOwnTodo([FromBody] AddTodoTaskForMeCommand todo,
            IMediator mediator, CancellationToken cancellationToken)
        {
            var item = await mediator.Send(todo, cancellationToken);
            return Created($"/todos/{item.Id}", item);
        }

        [HttpPut]
        [Route("todos")]
        public async Task<IActionResult> UpdateTodo([FromBody] EditTodoTaskCommand todo, 
            IMediator mediator, CancellationToken cancellationToken)
        {
            var item = await mediator.Send(todo, cancellationToken);
            return Ok(item);
        }

        [HttpPatch]
        [Route("/todos/IsDone")]
        public async Task<IActionResult> SetIsDoneField(SetIsDoneFieldCommand done, IMediator mediator, CancellationToken cancellationToken)
        {
            var item = await mediator.Send(done, cancellationToken);
            return Ok(new { Id = item.Id, IsDone = item.IsDone});
        }

        [HttpDelete]
        [Route("/todos/{id}")]
        public async Task<IActionResult> DeleteItem(long id, IMediator mediator, CancellationToken cancellationToken)
        {
            await mediator.Send(new DeleteTodoTaskCommand() {Id = id}, cancellationToken);
            return Ok();
        }
    }
}