﻿using Auth.Api.Mapper;
using Common.Domain;
using Common.Repo;
using FluentValidation;
using SharpGrip.FluentValidation.AutoValidation.Mvc.Extensions;
using System.Reflection;

namespace Auth.Api
{
    public static class AuthServiceDI
    {
        public static IServiceCollection AddAuthServices(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(AutoMapperProfile));
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IBaseRepository<AppUser>, PostgreSqlBaseRepository<AppUser>>();
            services.AddTransient<IBaseRepository<RefreshToken>, PostgreSqlBaseRepository<RefreshToken>>();
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddFluentValidationAutoValidation();
            return services;
        }
    }
}
