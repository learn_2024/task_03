﻿using Auth.Api.Dto;

namespace Auth.Api
{
    public interface IAuthService
    {
        Task<JwtDto> GetJwtTokenAsync(AuthDto authDto, CancellationToken cancellationToken);
        Task<JwtDto> GetJwtTokenByRefreshTokenAsync(string RefreshToken, CancellationToken cancellationToken);
    }
}