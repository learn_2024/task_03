﻿using Auth.Api.Dto;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Common.Repo;
using Common.Domain;
using Common.Api.Exceptions;
using Common.Utils;

namespace Auth.Api
{
    public class AuthService : IAuthService
    {
        private readonly IBaseRepository<AppUser> _userRep;
        private readonly IBaseRepository<RefreshToken> _refreshTokenRepo;
        private readonly IConfiguration _configuration;

        public AuthService(IBaseRepository<AppUser> userRep,
            IBaseRepository<RefreshToken> refreshTokenRepo,
            IConfiguration configuration)
        {
            _userRep = userRep;
            _refreshTokenRepo = refreshTokenRepo;
            _configuration = configuration;
        }
        public async Task<JwtDto> GetJwtTokenAsync(AuthDto authDto, CancellationToken cancellationToken)
        {

            var user = await _userRep.SingleOrDefaultAsync(p => p.Login == authDto.Login, cancellationToken);
            if (user is null)
            {
                throw new NotFoundException($"Пользователь с логином {authDto.Login} не существует.");
            }

            if (!PasswordHashUtil.VerifyPassword(authDto.Password, user.PasswordHash))
            {
                throw new ForbiddenException("Не верный пароль.");
            }

            var claims = new List<Claim>
            {
                new (ClaimTypes.Name, authDto.Login),
                new (ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            foreach(var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }
            //claims.AddRange(user.Roles.Select(role => new Claim(ClaimTypes.Role, role.Name)));
            //_configuration
            var securKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]!));
            var credentials = new SigningCredentials(securKey, SecurityAlgorithms.HmacSha256Signature);
            var expireDate = DateTime.UtcNow.AddMinutes(_configuration.GetValue<int>("Jwt:ExpireMinutes")); 
            var tokenDescriptor = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: expireDate, signingCredentials: credentials);
            var token = new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);
            var refreshToken = await _refreshTokenRepo.AddAsync(new RefreshToken() { UserId = user .Id}, cancellationToken);
            return new JwtDto()
            {
                Jwt = token,
                Expires = expireDate,
                RefreshToken = refreshToken.Id
            };
        }

        public async Task<JwtDto> GetJwtTokenByRefreshTokenAsync(string RefreshToken, CancellationToken cancellationToken)
        {
             var refreshToken = await _refreshTokenRepo.SingleOrDefaultAsync(p => p.Id == RefreshToken, cancellationToken);
             if (refreshToken is null)
             {
                 throw new ForbiddenException($"Forbidden");
             }

            var user = await _userRep.SingleOrDefaultAsync(p => p.Id == refreshToken.UserId, cancellationToken);

            var claims = new List<Claim>
             {
                 new (ClaimTypes.Name, user!.Login),
                 new (ClaimTypes.NameIdentifier, user.Id.ToString())
             };

             //claims.AddRange();
             //_configuration
             var securKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]!));
             var credentials = new SigningCredentials(securKey, SecurityAlgorithms.HmacSha256Signature);
             var expireDate = DateTime.UtcNow.AddMinutes(_configuration.GetValue<int>("Jwt:ExpireMinutes"));
             var tokenDescriptor = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: expireDate, signingCredentials: credentials);
             var token = new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);
             return new JwtDto()
             {
                 Jwt = token,
                 Expires = expireDate,
                 RefreshToken = refreshToken.Id
             };
        }
    }
}
