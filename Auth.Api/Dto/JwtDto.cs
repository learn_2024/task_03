﻿namespace Auth.Api.Dto
{
    public class JwtDto
    {
        public string Jwt { get; set; } = default!;
        public string RefreshToken { get; set; } = default!;
        public DateTime Expires { get; set; }

    }
}
