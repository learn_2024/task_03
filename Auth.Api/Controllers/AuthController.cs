using Auth.Service;
using Auth.Service.Dto;
using Common.App.Abstractions.Persistance;
using Common.Domain;
using Common.Repo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Auth.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IAuthService _authService;
        private readonly IBaseRepository<AppUser> _userRepo;

        public AuthController(ILogger<AuthController> logger, IAuthService authService, IBaseRepository<AppUser> userRepo)
        {
            _logger = logger;
            _authService = authService;
            _userRepo = userRepo;
        }

        [AllowAnonymous]
        [HttpPost("CreateJwt")]
        public async Task<IActionResult> CreateJwt(AuthDto authDto, CancellationToken cancellationToken)
        {
            var token = await _authService.GetJwtTokenAsync(authDto, cancellationToken);
            return Ok(token);
        }

        [AllowAnonymous]
        [HttpPost("CreateJwtByRefreshToken")]
        public async Task<IActionResult> CreateJwtByRefreshToken(string RefreshToken, CancellationToken cancellationToken)
        {
            var token = await _authService.GetJwtTokenByRefreshTokenAsync(RefreshToken, cancellationToken);
            return Ok(token);
        }

        [HttpGet("GetLoggedUserInfo")]
        public async Task<IActionResult> GetLoggedUserInfo(CancellationToken cancellationToken)
        {
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier)!;
            var user = await _userRepo.SingleOrDefaultAsync(p => p.Id == long.Parse(currentUserId.Value), cancellationToken);
            return Ok(user);
        }
    }
}
