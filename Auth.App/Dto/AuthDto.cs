﻿namespace Auth.Service.Dto
{
    public class AuthDto
    {
        public string Login { get; set; } = default!;
        public string Password { get; set; } = default!;
    }
}
