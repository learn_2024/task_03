﻿using Auth.Service.Dto;

namespace Auth.Service
{
    public interface IAuthService
    {
        Task<JwtDto> GetJwtTokenAsync(AuthDto authDto, CancellationToken cancellationToken);
        Task<JwtDto> GetJwtTokenByRefreshTokenAsync(string RefreshToken, CancellationToken cancellationToken);
    }
}