﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.App.Abstractions.Persistance
{
    public interface IContextTransactionStarter
    {
        Task<IContextTransaction> StartTransactionAsync(CancellationToken cancellationToken);
    }
}
