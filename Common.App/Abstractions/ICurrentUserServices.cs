﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.App.Abstractions
{
    public interface ICurrentUserServices
    {
        public string CurrentUserId { get; }
        public string[] CurrentUserRoles { get; }
        public bool IsAdmin { get; }
    }
}
