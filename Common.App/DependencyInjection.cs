﻿using Common.App.Behaviors;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Common.App
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommonApplication(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            return services;
        }
    }
}
