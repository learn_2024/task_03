﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Common.App.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(object filter) : base("Данные не найдены по параметрам: " + JsonSerializer.Serialize(filter))
        {

        }
    }
}
