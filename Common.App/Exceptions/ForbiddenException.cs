﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.App.Exceptions
{
    public class ForbiddenException : Exception
    {
        public ForbiddenException(string error) : base(error) { }
    }
}
