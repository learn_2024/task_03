﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.App.Exceptions;

public class BadRequestException : Exception
{
    public BadRequestException(string error) : base(error)
    {

    }
}

