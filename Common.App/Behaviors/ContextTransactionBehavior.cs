﻿using Common.App.Abstractions.Persistance;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.App.Behaviors
{
    public class ContextTransactionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private IContextTransactionStarter _contextTransactionStarter;

        public ContextTransactionBehavior(IContextTransactionStarter contextTransactionStarter) 
        {
            _contextTransactionStarter = contextTransactionStarter;
        }
        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            using (var transaction = await _contextTransactionStarter.StartTransactionAsync(cancellationToken))
            {
                try
                {
                    var result = await next();
                    await transaction.CommitAsync(cancellationToken);
                    return result;
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync(CancellationToken.None);
                    throw;
                }
            }
        }
    }
}
