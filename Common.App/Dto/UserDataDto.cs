﻿using Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.App.Dto
{
    public class UserDataDto
    {
        public long Id { get; set; }
        public string FIOUser { get; set; } = default!;
        public string Login { get; set; } = default!;
        public ICollection<AppUserRole> Roles { get; set; } = default!;
    }
}
