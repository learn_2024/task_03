﻿using Common.Api;
using Common.App.Abstractions;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharpGrip.FluentValidation.AutoValidation.Mvc.Extensions;
using System.Reflection;
using Common.App;
using Todo.App.Mapper;
using Common.Persistence;

namespace Todo.App
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddTodoServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddAutoMapper(typeof(AutoMapperProfile));
            services.AddApplicationDatabase(config);
            services.AddTransient<ICurrentUserServices, CurrentUserServices>();
            services.AddHttpContextAccessor();
            services.AddCommonApplication();
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddFluentValidationAutoValidation();
            services.AddHttpContextAccessor();
            services.AddSingleton<TodosMemoryCache>();
            services.AddMediatR(p => p.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
            return services;
        }
    }
}
