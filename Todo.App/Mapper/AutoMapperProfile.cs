﻿using AutoMapper;
using Common.Domain;
using Todo.App.Command.AddTodoTaskForAnyOwner;
using Todo.App.Command.AddTodoTaskForMe;
using Todo.App.Command.EditTodoTask;

namespace Todo.App.Mapper
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile() {
            CreateMap<AddTodoTaskForAnyOwnerCommand, TodoTask>();
            CreateMap<AddTodoTaskForMeCommand, TodoTask>();
            CreateMap<EditTodoTaskCommand, TodoTask>();
        }
    }
}
