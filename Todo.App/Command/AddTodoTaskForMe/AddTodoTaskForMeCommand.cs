﻿using Common.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Command.AddTodoTaskForMe
{
    public class AddTodoTaskForMeCommand: IRequest<TodoTask>
    {
        public string TodoLabel { get; set; } = default!;
        public bool IsDone { get; set; }
    }
}
