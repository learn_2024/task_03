﻿using AutoMapper;
using Common.Domain;
using Serilog;
using System.Text.Json;
using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using MediatR;
using System.Threading.Tasks;

namespace Todo.App.Command.AddTodoTaskForMe
{
    public class AddTodoTaskForMeCommandHandler: IRequestHandler<AddTodoTaskForMeCommand, TodoTask>
    {
        private readonly IBaseRepository<TodoTask> _todoRepo;
        private readonly IBaseRepository<AppUser> _userRepo;
        private readonly IMapper _mapper;
        private readonly ICurrentUserServices _currentUserServices;
        public AddTodoTaskForMeCommandHandler(IBaseRepository<TodoTask> todoRepo,
            IBaseRepository<AppUser> userRepo,
            IMapper mapper,
            ICurrentUserServices currentUserServices)
        {
            _todoRepo = todoRepo;
            _mapper = mapper;
            _currentUserServices = currentUserServices;
            _userRepo = userRepo;
        }

        public async Task<TodoTask> Handle(AddTodoTaskForMeCommand request, CancellationToken cancellationToken)
        {
            Log.Information("Добавление задачи");
            Log.Information(JsonSerializer.Serialize(request));

            var newItem = new TodoTask();
            newItem.OwnerId = long.Parse(_currentUserServices.CurrentUserId);
            _mapper.Map(request, newItem);
            return await _todoRepo.AddAsync(newItem, cancellationToken);
        }
    }
}
