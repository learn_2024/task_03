﻿using AutoMapper;
using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.Domain;
using MediatR;
using Serilog;
using System.Text.Json;
using System.Threading.Tasks;

namespace Todo.App.Command.AddTodoTaskForAnyOwner
{
    public class AddTodoTaskForAnyOwnerCommandHandler: IRequestHandler<AddTodoTaskForAnyOwnerCommand, TodoTask>
    {
        private readonly IBaseRepository<TodoTask> _todoRepo;
        private readonly IBaseRepository<AppUser> _userRepo;
        private readonly IMapper _mapper;
        private readonly ICurrentUserServices _currentUserServices;
        public AddTodoTaskForAnyOwnerCommandHandler(IBaseRepository<TodoTask> todoRepo, 
            IBaseRepository<AppUser> userRepo, 
            IMapper mapper, 
            ICurrentUserServices currentUserServices) 
        {
            _todoRepo = todoRepo;
            _mapper = mapper;
            _currentUserServices = currentUserServices;
            _userRepo = userRepo;
        }

        public async Task<TodoTask> Handle(AddTodoTaskForAnyOwnerCommand request, CancellationToken cancellationToken)
        {
            Log.Information("Добавление задачи");
            Log.Information(JsonSerializer.Serialize(request));

            if (_currentUserServices.IsAdmin)
            {
                throw new ForbiddenException("Недостаточно прав для данной операции");
            }
            ;
            if (await _userRepo.SingleOrDefaultAsync(x => x.Id == request.OwnerId, cancellationToken) == null)
                throw new BadRequestException($"Пользователь с id = {request.OwnerId} не найден");

            var newItem = new TodoTask();
            _mapper.Map(request, newItem);
            return await _todoRepo.AddAsync(newItem, cancellationToken);
        }
    }

}
