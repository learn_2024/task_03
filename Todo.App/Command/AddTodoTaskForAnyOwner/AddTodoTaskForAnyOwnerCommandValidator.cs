﻿using FluentValidation;
using Todo.App.Command.AddTodoTaskForAnyOwner;

namespace Todo.App.Validation
{
    public class AddTodoTaskForAnyOwnerCommandValidator: AbstractValidator<AddTodoTaskForAnyOwnerCommand>
    {
        public AddTodoTaskForAnyOwnerCommandValidator() 
        {
            RuleFor(p => p.OwnerId).GreaterThanOrEqualTo(1).WithMessage("OwnerID должно быть больше 0");
            RuleFor(p => p.TodoLabel).MinimumLength(5).MaximumLength(255).WithMessage("Длина текста задачидолжна быть от 5 до 255 символов");

        }
    }
}
