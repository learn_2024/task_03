﻿using Common.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Command.EditTodoTask
{
    public class EditTodoTaskCommand: IRequest<TodoTask>
    {
        public long TaskId { get; set; }
        public string TodoLabel { get; set; } = default!;
        public bool IsDone { get; set; }
        public long OwnerId { get; set; }
    }
}
