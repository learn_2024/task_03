﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Command.EditTodoTask
{
    public class EditTodoTaskCommandValidator : AbstractValidator<EditTodoTaskCommand>
    {
        public EditTodoTaskCommandValidator()
        {
            RuleFor(p => p.OwnerId).GreaterThanOrEqualTo(1).WithMessage("OwnerID должно быть больше 0");
            RuleFor(p => p.TodoLabel).MinimumLength(5).MaximumLength(255).WithMessage("Длина текста задачидолжна быть от 5 до 255 символов");
            RuleFor(p => p.IsDone).Equal(true).WithMessage("Редактировать можно только не завершенные задачи.");
        }
    }
}
