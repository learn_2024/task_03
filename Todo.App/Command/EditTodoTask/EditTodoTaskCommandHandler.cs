﻿using AutoMapper;
using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.Domain;
using MediatR;
using Serilog;
using System.Text.Json;
using System.Threading.Tasks;

namespace Todo.App.Command.EditTodoTask
{
    public class EditTodoTaskCommandHandler: IRequestHandler<EditTodoTaskCommand, TodoTask>
    {

        private readonly IBaseRepository<TodoTask> _todoRepo;
        private readonly IBaseRepository<AppUser> _userRepo;
        private readonly IMapper _mapper;
        private readonly ICurrentUserServices _currentUserServices;
        public EditTodoTaskCommandHandler(IBaseRepository<TodoTask> todoRepo,
            IBaseRepository<AppUser> userRepo,
            IMapper mapper,
            ICurrentUserServices currentUserServices)
        {
            _todoRepo = todoRepo;
            _mapper = mapper;
            _currentUserServices = currentUserServices;
            _userRepo = userRepo;
        }

        public async Task<TodoTask> Handle(EditTodoTaskCommand request, CancellationToken cancellationToken)
        {
            Log.Information($"Изменение задачи. ID = {request.TaskId}");
            Log.Information(JsonSerializer.Serialize(request));
            
            var editedItem = await _todoRepo.SingleOrDefaultAsync(x => x.Id == request.TaskId, cancellationToken);
            if (editedItem == null)
            {
                throw new NotFoundException(new { TodoTaskId = request.TaskId });
            }

            if ((_currentUserServices.CurrentUserId != editedItem.OwnerId.ToString()) && (!_currentUserServices.IsAdmin))
            {
                throw new ForbiddenException("Не достаточно прав для операции.");
            }

            _mapper.Map(request, editedItem);
            editedItem.UpdatedDate = DateTime.UtcNow;
            return await _todoRepo.UpdateAsync(editedItem, cancellationToken);
        }
    }
}
