﻿using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.Domain;
using MediatR;
using Serilog;
using Todo.App.Command.DeleteTodoTask;

namespace Todo.App.Command.DeleteTodoTask
{
    public class DeleteTodoTaskCommandHandler: IRequestHandler<DeleteTodoTaskCommand>
    {
        private readonly IBaseRepository<TodoTask> _todoRepo;
        private readonly ICurrentUserServices _currentUserServices;
        public DeleteTodoTaskCommandHandler(IBaseRepository<TodoTask> todoRepo,
            ICurrentUserServices currentUserServices)
        {
            _todoRepo = todoRepo;
            _currentUserServices = currentUserServices;
        }

        public async Task Handle(DeleteTodoTaskCommand request, CancellationToken cancellationToken)
        {
            Log.Information(String.Format("Удаление задачи. ID = {0}", request.Id));

            var item = await _todoRepo.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (item == null)
            {
                throw new NotFoundException(new { TodoTaskId = request.Id });
            }

            if ((_currentUserServices.CurrentUserId != item.OwnerId.ToString()) && (!_currentUserServices.IsAdmin))
            {
                throw new ForbiddenException("Не достаточно прав для операции.");
            }

            await _todoRepo.DeleteAsync(item, cancellationToken);
        }

    }
}
