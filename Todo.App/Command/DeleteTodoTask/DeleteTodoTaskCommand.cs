﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Command.DeleteTodoTask
{
    public class DeleteTodoTaskCommand: IRequest
    {
        public long Id { get; set; }
    }
}
