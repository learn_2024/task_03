﻿using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.Domain;
using MediatR;
using Serilog;
using System.Runtime.CompilerServices;
using System.Text.Json;
using Todo.App.Command.SetIsDoneField;

namespace Todo.App.Command.SetIsDoneField
{
    public class SetIsDoneFieldCommandHandler: IRequestHandler<SetIsDoneFieldCommand, TodoTask>
    {
        private readonly IBaseRepository<TodoTask> _todoRepo;
        private readonly ICurrentUserServices _currentUserServices;
        public SetIsDoneFieldCommandHandler(IBaseRepository<TodoTask> todoRepo,
            ICurrentUserServices currentUserServices)
        {
            _todoRepo = todoRepo;
            _currentUserServices = currentUserServices;
        }

        public async Task<TodoTask> Handle(SetIsDoneFieldCommand request, CancellationToken cancellationToken)
        {
            Log.Information("Установка IsDone");
            Log.Information(JsonSerializer.Serialize(request));
            var item = await _todoRepo.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (item == null)
            {
                throw new NotFoundException(new { TodoTaskId = request.Id });
            }

            if ((_currentUserServices.CurrentUserId != item.OwnerId.ToString()) && (!_currentUserServices.IsAdmin))
            {
                throw new ForbiddenException("Не достаточно прав для операции.");
            }

            item.IsDone = request.IsDone;
            item.UpdatedDate = DateTime.UtcNow;
            return await _todoRepo.UpdateAsync(item, cancellationToken);
        }

    }
}
