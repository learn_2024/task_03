﻿using Common.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Command.SetIsDoneField
{
    public class SetIsDoneFieldCommand: IRequest<TodoTask>
    {
        public long Id { get; set; }
        public bool IsDone { get; set; }
    }
}
