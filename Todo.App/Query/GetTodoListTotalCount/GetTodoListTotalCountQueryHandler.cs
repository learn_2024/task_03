﻿using Common.Domain;
using Microsoft.Extensions.Caching.Memory;
using System.Text.Json.Serialization;
using System.Text.Json;
using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using MediatR;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Todo.App.Query.GetTodoListTotalCount
{
    public class GetTodoListTotalCountQueryHandler: IRequestHandler<GetTodoListTotalCountQuery, int>
    {
        private readonly IBaseRepository<TodoTask> _todosRepo;
        private readonly MemoryCache _cache;
        private readonly ICurrentUserServices _currentUserServices;

        public GetTodoListTotalCountQueryHandler(IBaseRepository<TodoTask> todosRepo, TodosMemoryCache cache, ICurrentUserServices currentUserServices)
        {
            _todosRepo = todosRepo;
            _cache = cache.Cache;
            _currentUserServices = currentUserServices;
        }

        public async Task<int> Handle(GetTodoListTotalCountQuery request, CancellationToken cancellationToken)
        {
            int? localOwnerId = _currentUserServices.IsAdmin ? request.OwnerId : int.Parse(_currentUserServices.CurrentUserId);
            request.OwnerId = localOwnerId;

            var cacheKey = JsonSerializer.Serialize($"TodoTotalCount:{request}", new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.IgnoreCycles });

            if (_cache.TryGetValue(cacheKey, out int? result))
            {
                return result!.Value;
            }

            result = await _todosRepo.CountAsync((request.TaskName == null) && (localOwnerId == null) ? null : p => (((request.TaskName != null) && (p.TodoLabel.Contains(request.TaskName, StringComparison.InvariantCultureIgnoreCase))) || (request.TaskName == null)) &&
                                                                     (((localOwnerId != null) && (p.OwnerId == localOwnerId)) || (localOwnerId == null)), cancellationToken);

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(5))
                .SetSize(10);

            _cache.Set(cacheKey, result, cacheEntryOptions);

            return result.Value;
        }
    }
}
