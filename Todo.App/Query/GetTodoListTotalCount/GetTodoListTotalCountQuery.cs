﻿using MediatR;
using Todo.App.Dto;

namespace Todo.App.Query.GetTodoListTotalCount
{
    public class GetTodoListTotalCountQuery: BaseTodoQueryFilter, IRequest<int>
    {
    }
}
