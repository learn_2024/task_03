﻿using Common.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Query.GetTodoTaskByID
{
    public class GetTodoTaskByIDCommand: IRequest<TodoTask>
    {
        public long Id { get; set; }
    }
}
