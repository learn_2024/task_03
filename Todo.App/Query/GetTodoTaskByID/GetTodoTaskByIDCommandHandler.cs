﻿using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.Domain;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System.Threading;
using Todo.App.Query.GetTodoTaskByID;


namespace Todo.App.Query.GetTodoTaskByID
{
    public class GetTodoTaskByIDCommandHandler: IRequestHandler<GetTodoTaskByIDCommand, TodoTask>
    {
        private readonly IBaseRepository<TodoTask> _todosRepo;
        private readonly MemoryCache _cache;
        private readonly ICurrentUserServices _currentUserServices;

        public GetTodoTaskByIDCommandHandler(IBaseRepository<TodoTask> todosRepo, TodosMemoryCache cache, ICurrentUserServices currentUserServices)
        {
            _todosRepo = todosRepo;
            _cache = cache.Cache;
            _currentUserServices = currentUserServices;
        }

        public async Task<TodoTask> Handle(GetTodoTaskByIDCommand request, CancellationToken cancellationToken)
        {
            var todo = await _todosRepo.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (todo != null)
            {
                if ((_currentUserServices.IsAdmin) ||
                    ((!_currentUserServices.IsAdmin) && (long.Parse(_currentUserServices.CurrentUserId) == todo.OwnerId)))
                { 
                    return todo; 
                }
                else 
                { 
                    throw new NotFoundException(new { Id = request.Id }); 
                }

            }
            else
            {
                throw new NotFoundException(new { Id = request.Id });
            }
        }
    }
}
