﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Query.GetList
{
    public class GetTodoListQueryValidator: AbstractValidator<GetTodoListQuery>
    {
        public GetTodoListQueryValidator() 
        {
            RuleFor(v => v.Limit).GreaterThan(0).When(v => v.Limit.HasValue);
            RuleFor(v => v.Offset).GreaterThan(0).When(v => v.Offset.HasValue);
            RuleFor(v => v.TaskName).MaximumLength(100);
        }
    }
}
