﻿using Common.Domain;
using MediatR;
using Todo.App.Dto;

namespace Todo.App.Query.GetList
{
    public class GetTodoListQuery: BaseTodoQueryFilter, IRequest<IReadOnlyCollection<TodoTask>>
    {
        public int? Offset { get; set; }
        public int? Limit { get; set; }

    }
}
