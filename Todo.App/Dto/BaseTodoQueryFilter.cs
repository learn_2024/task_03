﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.App.Dto
{
    public class BaseTodoQueryFilter
    {
        public string? TaskName { get; set; }
        public int? OwnerId { get; set; }
    }
}
