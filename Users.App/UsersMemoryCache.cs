﻿using Microsoft.Extensions.Caching.Memory;

namespace Users.App
{
    public class UsersMemoryCache
    {
        public MemoryCache Cache { get; } = new MemoryCache(new MemoryCacheOptions() { SizeLimit = 1024 });
    }
} 
