﻿using FluentValidation;

namespace Users.App.Command.EditUser
{
    public class EditUserCommandValidator : AbstractValidator<EditUserCommand>
    {
        public EditUserCommandValidator()
        {
            RuleFor(p => p.FIOUser).MinimumLength(4).MaximumLength(50).WithMessage("Длина имени пользователя должна быть от 4 до 50 символов ");
            RuleFor(p => p.Login).MinimumLength(4).MaximumLength(50).WithMessage("Длина логина пользователя должна быть от 4 до 50 символов ");

        }
    }
}
