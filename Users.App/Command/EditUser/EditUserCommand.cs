﻿using Common.App.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.App.Command.EditUser
{
    public class EditUserCommand: IRequest<UserDataDto>
    {
        public long Id { get; set; }
        public string FIOUser { get; set; } = default!;
        public string Login { get; set; } = default!;
    }
}
