﻿using AutoMapper;
using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Dto;
using Common.App.Exceptions;
using Common.Domain;
using MediatR;
using Serilog;
using System.Text.Json;

namespace Users.App.Command.EditUser
{
    public class EditUserCommandHandler: IRequestHandler<EditUserCommand, UserDataDto>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly IMapper _mapper;
        private readonly ICurrentUserServices _currentUserServices;
        public EditUserCommandHandler(IBaseRepository<AppUser> usersRepo, IMapper mapper, ICurrentUserServices currentUserServices)
        {
            _usersRepo = usersRepo;
            _mapper = mapper;
            _currentUserServices = currentUserServices;
        }

        public async Task<UserDataDto> Handle(EditUserCommand request, CancellationToken cancellationToken)
        {
            Log.Information($"Изменение пользователя. ID = {request.Id}");
            Log.Information(JsonSerializer.Serialize(request));

            if (_currentUserServices.CurrentUserId != request.Id.ToString() && !_currentUserServices.IsAdmin)
            {
                throw new ForbiddenException("Нет прав для данной операции");
            }


            if (await _usersRepo.SingleOrDefaultAsync(p => p.Login == request.Login.Trim()) is not null)
            {
                throw new BadRequestException($"Пользователь '{request.Login}' уже существует");
            }
            var editedItem = await _usersRepo.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (editedItem == null)
            {
                throw new NotFoundException(new { UserId = request.Id });
            }

            _mapper.Map(request, editedItem);
            return _mapper.Map<UserDataDto>(await _usersRepo.UpdateAsync(editedItem, cancellationToken));
        }

    }
}
