﻿using AutoMapper;
using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Dto;
using Common.App.Exceptions;
using Common.App.Utils;
using Common.Domain;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Serilog;
using System.Text.Json;

namespace Users.App.Command.AddUser
{
    public class AddUserCommandHandler: IRequestHandler<AddUserCommand, UserDataDto>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly IBaseRepository<AppUserRole> _rolesRepo;
        private readonly IMapper _mapper;
        private readonly ICurrentUserServices _currentUserServices;
        private readonly MemoryCache _cache;

        public AddUserCommandHandler(IBaseRepository<AppUser> usersRepo, IBaseRepository<AppUserRole> rolesRepo, 
            IMapper mapper, ICurrentUserServices currentUserServices, UsersMemoryCache cache)
        {
            _usersRepo = usersRepo;
            _rolesRepo = rolesRepo;
            _mapper = mapper;
            _currentUserServices = currentUserServices;
            _cache = cache.Cache;
        }

        public async Task<UserDataDto> Handle(AddUserCommand request, CancellationToken cancellationToken)
        {
            Log.Information("Добавление пользователя");
            Log.Information(JsonSerializer.Serialize(request));

            if (!_currentUserServices.IsAdmin)
            {
                throw new ForbiddenException("Нет прав для данной операции");
            }

            if (await _usersRepo.SingleOrDefaultAsync(p => p.Login == request.Login.Trim()) is not null)
            {
                throw new BadRequestException($"Пользователь '{request.Login}' уже существует");
            }

            var clientRole = await _rolesRepo.SingleOrDefaultAsync(r => r.Name == "Client");

            var newItem = new AppUser()
            {
                FIOUser = request.FIOUser.Trim(),
                Login = request.Login.Trim(),
                PasswordHash = PasswordHashUtil.HashPassword(request.Password),
                Roles = new List<AppUserRole>()
            };
            newItem.Roles.Add(clientRole!);

            var result = _mapper.Map<UserDataDto>(await _usersRepo.AddAsync(newItem, cancellationToken));
            //_cache.Clear(); 

            return result;

        }

    }
}
