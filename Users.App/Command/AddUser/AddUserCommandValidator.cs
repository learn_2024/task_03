﻿using FluentValidation;

namespace Users.App.Command.AddUser
{
    public class AddUserCommandValidator : AbstractValidator<AddUserCommand>
    {
        public AddUserCommandValidator()
        {
            RuleFor(p => p.FIOUser).MinimumLength(4).MaximumLength(50).WithMessage("Длина имени пользователя должна быть от 4 до 50 символов ").
                NotEmpty().WithMessage("Необходимо заполнить имя пользователя");
            RuleFor(p => p.Login).MinimumLength(4).MaximumLength(50).WithMessage("Длина логина пользователя должна быть от 4 до 50 символов ").
                NotEmpty().WithMessage("Необходимо заполнить логин"); ;
            RuleFor(p => p.Password).MinimumLength(6).MaximumLength(50).WithMessage("Длина пароля должна быть от 6 до 50 символов ").
                NotEmpty().WithMessage("Необходимо заполнить указать пароль"); ;
        }
    }

}
