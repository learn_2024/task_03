﻿using Common.App.Dto;
using MediatR;

namespace Users.App.Command.AddUser
{
    public class AddUserCommand: IRequest<UserDataDto>
    {
        public string FIOUser { get; set; } = default!;
        public string Login { get; set; } = default!;
        public string Password { get; set; } = default!;
    }
}
