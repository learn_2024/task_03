﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.App.Command.ResetPassword
{
    public class ResetPasswordCommand: IRequest
    {
        public long IdUser { get; set; }
        public string NewPassword { get; set; } = default!;
    }
}
