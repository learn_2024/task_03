﻿using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.App.Utils;
using Common.Domain;
using MediatR;
using Serilog;
using System.Text.Json;

namespace Users.App.Command.ResetPassword
{
    public class ResetPasswordCommandHandler: IRequestHandler<ResetPasswordCommand>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly ICurrentUserServices _currentUserServices;

        public ResetPasswordCommandHandler(IBaseRepository<AppUser> usersRepo, ICurrentUserServices currentUserServices)
        {
            _usersRepo = usersRepo;
            _currentUserServices = currentUserServices;
        }

        public async Task Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            Log.Information($"Установка нового пароля. ID = {request.IdUser}");
            Log.Information(JsonSerializer.Serialize(request));

            if (!_currentUserServices.IsAdmin)
            {
                throw new ForbiddenException("Нет прав для данной операции");
            }

            var editedItem = await _usersRepo.SingleOrDefaultAsync(x => x.Id == request.IdUser, cancellationToken);
            if (editedItem == null)
            {
                throw new NotFoundException(new { Id = request.IdUser });
            }

            editedItem.PasswordHash = PasswordHashUtil.HashPassword(request.NewPassword);
            await _usersRepo.UpdateAsync(editedItem, cancellationToken);
        }
    }
}
