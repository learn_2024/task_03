﻿using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.App.Utils;
using Common.Domain;
using MediatR;
using Serilog;
using System.Text.Json;

namespace Users.App.Command.ChangePassword
{
    public class ChangeUserPasswordCommandHandler: IRequestHandler<ChangeUserPasswordCommand, bool>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly ICurrentUserServices _currentUserServices;

        public ChangeUserPasswordCommandHandler(IBaseRepository<AppUser> usersRepo, ICurrentUserServices currentUserServices)
        {
            _usersRepo = usersRepo;
            _currentUserServices = currentUserServices;
        }

        public async Task<bool> Handle(ChangeUserPasswordCommand request, CancellationToken cancellationToken)
        {
            Log.Information($"Изменение пароля. ID = {request.UserId}");
            Log.Information(JsonSerializer.Serialize(request));

            if (_currentUserServices.CurrentUserId != request.UserId.ToString() && !_currentUserServices.IsAdmin)
            {
                throw new ForbiddenException("Нет прав для данной операции");
            }

            var editedItem = await _usersRepo.SingleOrDefaultAsync(x => x.Id == request.UserId, cancellationToken);
            if (editedItem == null)
            {
                throw new NotFoundException(new { Id = request.UserId });
            }

            if (!PasswordHashUtil.VerifyPassword(request.OldPassword, editedItem.PasswordHash))
            {
                throw new ForbiddenException("Старый пароль не верен!");
            }
            else
            {
                editedItem.PasswordHash = PasswordHashUtil.HashPassword(request.NewPassword);
                await _usersRepo.UpdateAsync(editedItem, cancellationToken);
                return true;
            }
        }

    }
}
