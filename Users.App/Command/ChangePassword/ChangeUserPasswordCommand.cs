﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.App.Command.ChangePassword
{
    public class ChangeUserPasswordCommand: IRequest<bool>
    {
        public long UserId { get; set; }
        public string OldPassword { get; set; } = default!;
        public string NewPassword { get; set; } = default!;

    }
}
