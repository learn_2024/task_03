﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.App.Command.DeleteUser
{
    public class DeleteUserCommand: IRequest
    {
        public long Id {  get; set; }
    }
}
