﻿using Common.App.Abstractions;
using Common.App.Abstractions.Persistance;
using Common.App.Exceptions;
using Common.Domain;
using MediatR;
using Serilog;

namespace Users.App.Command.DeleteUser
{
    public class DeleteUserCommandHandler: IRequestHandler<DeleteUserCommand>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly ICurrentUserServices _currentUserServices;
        public DeleteUserCommandHandler(IBaseRepository<AppUser> usersRepo, ICurrentUserServices currentUserServices)
        {
            _usersRepo = usersRepo;
            _currentUserServices = currentUserServices;
        }

        public async Task Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            Log.Information(string.Format("Удаление пользователя. ID = {0}", request.Id));
            if (!_currentUserServices.IsAdmin)
            {
                throw new ForbiddenException("Нет прав для данной операции");
            }

            var user = await _usersRepo.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (user == null)
            {
                throw new NotFoundException(new { OwnerId = request.Id });
            }
            await _usersRepo.DeleteAsync(user, cancellationToken);
        }

    }
}
