﻿using AutoMapper;
using Common.App.Dto;
using Common.Domain;
using Users.App.Command.EditUser;


namespace Users.App.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<EditUserCommand, AppUser>();
            CreateMap<AppUser, UserDataDto>();
        }
    }
}
