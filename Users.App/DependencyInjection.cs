﻿using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Users.App.Mapper;
using Common.App.Abstractions;
using Common.Api;
using Microsoft.Extensions.Configuration;
using FluentValidation;
using Common.App;
using Common.Persistence;

namespace Users.App
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddUserApplication(this IServiceCollection services, IConfiguration config)
        {
            services.AddAutoMapper(typeof(AutoMapperProfile));
            services.AddSingleton<UsersMemoryCache>();
            services.AddTransient<ICurrentUserServices, CurrentUserServices>();
            services.AddApplicationDatabase(config);
            services.AddHttpContextAccessor();
            services.AddCommonApplication();
            services.AddMediatR(p=>p.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly())); 
            services.AddValidatorsFromAssemblies(new[] {Assembly.GetExecutingAssembly()}, includeInternalTypes: true);
            return services;
        }
    }
}
