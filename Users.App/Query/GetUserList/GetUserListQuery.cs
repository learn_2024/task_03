﻿using Common.App.Dto;
using MediatR;
using System.Collections.ObjectModel;

namespace Users.App.Query.GetUserList
{
    public class GetUserListQuery : BaseUserQueryFilter, IRequest<IReadOnlyCollection<UserDataDto>>
    {
        public int? Offset { get; set; }
        public int? Limit { get; set; }
    }
}
