﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.App.Query.GetUserList
{
    public class GetUserListQueryValidator : AbstractValidator<GetUserListQuery>
    {
        public GetUserListQueryValidator()
        {
            RuleFor(v => v.Limit).GreaterThan(0).When(v => v.Limit.HasValue);
            RuleFor(v => v.Offset).GreaterThan(0).When(v => v.Offset.HasValue);
            RuleFor(v => v.FioUser).MaximumLength(100);
        }
    }
}
