﻿using AutoMapper;
using Common.App.Abstractions.Persistance;
using Common.App.Dto;
using Common.Domain;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.ObjectModel;
using System.Text.Json;
using System.Text.Json.Serialization;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Users.App.Query.GetUserList
{
    public class GetUserListQueryHandler: IRequestHandler<GetUserListQuery, IReadOnlyCollection<UserDataDto>>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly IMapper _mapper;
        private readonly MemoryCache _cache;

        public GetUserListQueryHandler(IBaseRepository<AppUser> usersRepo, IMapper mapper, UsersMemoryCache cache)
        {
            _usersRepo = usersRepo;
            _mapper = mapper;
            _cache = cache.Cache;
        }

        public async Task<IReadOnlyCollection<UserDataDto>> Handle(GetUserListQuery request, CancellationToken cancellationToken)
        {
            var cacheKey = JsonSerializer.Serialize(request, new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.IgnoreCycles });
            if (_cache.TryGetValue(cacheKey, out IReadOnlyCollection<UserDataDto>? result))
            {
                return result!;
            }
            result = _mapper.Map<IReadOnlyCollection<UserDataDto>>(await _usersRepo.GetListAsync(request.Offset,
            request.Limit,
                request.FioUser == null ? null : p => p.FIOUser.Contains(request.FioUser, StringComparison.InvariantCultureIgnoreCase),
                p => p.Id, null,
                cancellationToken));
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(5))
                .SetSize(10);

            _cache.Set(cacheKey, result, cacheEntryOptions);

            return result;
        }
    }
}
