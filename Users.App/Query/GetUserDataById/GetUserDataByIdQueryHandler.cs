﻿using AutoMapper;
using Common.App.Abstractions.Persistance;
using Common.App.Dto;
using Common.Domain;
using MediatR;
using Microsoft.Extensions.Caching.Memory;

namespace Users.App.Query.GetUserDataById
{
    public class GetUserDataByIdQueryHandler: IRequestHandler<GetUserDataByIdQuery, UserDataDto>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly IMapper _mapper;
        private readonly MemoryCache _cache;

        public GetUserDataByIdQueryHandler(IBaseRepository<AppUser> usersRepo, IMapper mapper, UsersMemoryCache cache)
        {
            _usersRepo = usersRepo;
            _mapper = mapper;
            _cache = cache.Cache;
        }

        public async Task<UserDataDto> Handle(GetUserDataByIdQuery request, CancellationToken cancellationToken)
        {
            var cacheKey = "GetUserDataByIdQueryHandler_" + request.Id.ToString();

            if (_cache.TryGetValue(cacheKey, out UserDataDto? result))
            {
                return result!;
            }

            result = _mapper.Map<UserDataDto>(await _usersRepo.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken));

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(5))
                .SetSize(1);

            _cache.Set(cacheKey, result, cacheEntryOptions);

            return result;
        }

    }
}
