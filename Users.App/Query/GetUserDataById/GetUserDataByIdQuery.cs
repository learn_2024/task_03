﻿using Common.App.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.App.Query.GetUserDataById
{
    public class GetUserDataByIdQuery: IRequest<UserDataDto>
    {
        public long Id { get; set; }
    }
}
