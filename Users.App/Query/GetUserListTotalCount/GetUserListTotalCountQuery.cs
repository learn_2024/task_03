﻿using Common.App.Dto;
using MediatR;

namespace Users.App.Query.GetUserListTotalCount
{
    public class GetUserListTotalCountQuery : BaseUserQueryFilter, IRequest<int>
    {
    }
}
