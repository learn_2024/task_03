﻿using Common.Domain;
using Microsoft.Extensions.Caching.Memory;
using System.Text.Json.Serialization;
using System.Text.Json;
using Common.App.Abstractions.Persistance;
using MediatR;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Users.App.Query.GetUserListTotalCount
{
    public class GetUserListTotalCountQueryHandler: IRequestHandler<GetUserListTotalCountQuery, int>
    {
        private readonly IBaseRepository<AppUser> _usersRepo;
        private readonly MemoryCache _cache;
        public GetUserListTotalCountQueryHandler(IBaseRepository<AppUser> usersRepo, UsersMemoryCache cache)
        {
            _usersRepo = usersRepo;
            _cache = cache.Cache;
        }

        public async Task<int> Handle(GetUserListTotalCountQuery request, CancellationToken cancellationToken)
        {
            var cacheKey = JsonSerializer.Serialize($"UserTotalCount{request}", new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.IgnoreCycles });
            if (_cache.TryGetValue(cacheKey, out int? result))
            {
                return result!.Value;
            }
            result = await _usersRepo.CountAsync(request.FioUser == null ? null : p => p.FIOUser.Contains(request.FioUser, StringComparison.InvariantCultureIgnoreCase), cancellationToken);

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(5))
                .SetSize(1);

            _cache.Set(cacheKey, result.Value, cacheEntryOptions);

            return result.Value;
        }
    }
}
