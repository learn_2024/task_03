﻿namespace Common.Domain
{
    public class AppUser
    {
        public long Id { get; set; }
        public string FIOUser { get; set; } = default!;
        public string Login { get; set; } = default!;
        public string PasswordHash { get; set; } = default!;
        public ICollection<TodoTask>? Tasks { get; set; }
        public ICollection<AppUserRole> Roles { get; set; } = default!;
    }
}