﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Domain
{
    public class AppUserRole
    {
        public long Id { get; set; }
        public string Name { get; set; } = default!;
        public ICollection<AppUser> Users { get; set; } = default!;
    }
}
