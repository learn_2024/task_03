﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Domain
{
    public class RefreshToken
    {
        public string Id { get; set; } = default!;
        public long UserId {  get; set; }
        public AppUser User { get; set; } = default!;
    }
}
