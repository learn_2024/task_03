﻿using Common.App.Abstractions.Persistance;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Common.Persistence
{
    public class PostgreSqlBaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, new()
    {
        private readonly ApplicationDbContext _DbContext;

        public PostgreSqlBaseRepository(ApplicationDbContext DbContext)
        {
            _DbContext = DbContext;
        }

        public async Task<TEntity[]> GetListAsync(int? offset = null, int? limit = null,
            Expression<Func<TEntity, bool>>? predicate = null,
            Expression<Func<TEntity, object>>? orderBy = null,
            bool? descending = null,
            CancellationToken cancellationToken = default)
        {
            var queryable = _DbContext.Set<TEntity>().AsQueryable();

            if (predicate is not null)
            {
                queryable = queryable.Where(predicate);
            }

            if (orderBy is not null)
            {
                queryable = descending == true ? queryable.OrderByDescending(orderBy) : queryable.OrderBy(orderBy);
            }

            if (offset.HasValue)
            {
                queryable = queryable.Skip(offset.Value);
            }

            if (limit.HasValue)
            {
                queryable = queryable.Take(limit.Value);
            }

            return await queryable.ToArrayAsync(cancellationToken);
        }

        public async Task<TEntity?> SingleOrDefaultAsync(Expression<Func<TEntity, bool>>? predicate = null, CancellationToken cancellationToken = default)
        {
            var set = _DbContext.Set<TEntity>();
            return predicate == null ? await set.SingleOrDefaultAsync(cancellationToken) : await set.SingleOrDefaultAsync(predicate, cancellationToken);
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>>? predicate = null, CancellationToken cancelationToken = default)
        {
            var set = _DbContext.Set<TEntity>();
            return predicate == null ? await set.CountAsync(cancelationToken) : await set.CountAsync(predicate, cancelationToken);
        }

        public async Task<TEntity> AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            var set = _DbContext.Set<TEntity>();
            set.Add(entity);
            await _DbContext.SaveChangesAsync(cancellationToken);
            return entity;
        }

        public async Task<TEntity> UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            var set = _DbContext.Set<TEntity>();
            set.Update(entity);
            await _DbContext.SaveChangesAsync(cancellationToken);
            return entity;
        }

        public async Task<bool> DeleteAsync(TEntity entity, CancellationToken cancellationToken)
        {
            var set = _DbContext.Set<TEntity>();
            set.Remove(entity);
            return await _DbContext.SaveChangesAsync(cancellationToken) > 0;
        }
    }
}
