﻿using Common.App.Abstractions.Persistance;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Persistence
{
    public class ContextTransactionStarter : IContextTransactionStarter
    {
        private ApplicationDbContext _dbContext;

        public ContextTransactionStarter(ApplicationDbContext dbContext) 
        {
            _dbContext = dbContext;
        }
        public async Task<IContextTransaction> StartTransactionAsync(CancellationToken cancellationToken)
        {
            return new ContextTransaction(await _dbContext.Database.BeginTransactionAsync(cancellationToken));
       
        }
    }
}
