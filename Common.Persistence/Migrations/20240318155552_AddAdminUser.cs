﻿using Common.App.Utils;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Common.Repo.Migrations
{
    /// <inheritdoc />
    public partial class AddAdminUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(table: "Users",
                columns: new[] { "Id", "FIOUser", "Login", "PasswordHash" },
                values: new Object[,]
                {
                    { 0, "Admin", "Admin", PasswordHashUtil.HashPassword("12345678")}
                });

            migrationBuilder.InsertData(table: "AppUserAppUserRole",
                columns: new[] { "RolesId", "UsersId"},
                values: new Object[,]
                {
                    { 1, 0}
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
