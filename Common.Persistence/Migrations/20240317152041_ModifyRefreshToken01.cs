﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Common.Repo.Migrations
{
    /// <inheritdoc />
    public partial class ModifyRefreshToken01 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RefreshTokens_Users_UserItemId",
                table: "RefreshTokens");

            migrationBuilder.DropIndex(
                name: "IX_RefreshTokens_UserItemId",
                table: "RefreshTokens");

            migrationBuilder.DropColumn(
                name: "UserItemId",
                table: "RefreshTokens");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserItemId",
                table: "RefreshTokens",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_UserItemId",
                table: "RefreshTokens",
                column: "UserItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_RefreshTokens_Users_UserItemId",
                table: "RefreshTokens",
                column: "UserItemId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
