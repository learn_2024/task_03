﻿using Common.Domain;
using Microsoft.EntityFrameworkCore;

namespace Common.Persistence;
public class ApplicationDbContext : DbContext
{
    public DbSet<TodoTask> Todos { get; set; }
    public DbSet<AppUser> Users { get; set; }
    public DbSet<RefreshToken> RefreshTokens { get; set; }
    public DbSet<AppUserRole> AppUserRoles { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> dbContextOptions) : base(dbContextOptions)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AppUserRole>().HasKey(u => u.Id);
        //modelBuilder.Entity<AppUserRole>().Navigation(n => n.Users).AutoInclude();

        modelBuilder.Entity<TodoTask>().HasKey(c => c.Id);
        modelBuilder.Entity<TodoTask>().Property(b => b.TodoLabel).HasMaxLength(100).IsRequired();
        modelBuilder.Entity<TodoTask>().Property(b => b.CreatedDate).IsRequired();


        modelBuilder.Entity<AppUser>().HasKey(u => u.Id);
        modelBuilder.Entity<AppUser>().Property(b => b.FIOUser).HasMaxLength(50).IsRequired();
        modelBuilder.Entity<AppUser>().Property(b => b.Login).HasMaxLength(50).IsRequired();
        modelBuilder.Entity<AppUser>().Property(b => b.PasswordHash).HasMaxLength(1024).IsRequired();
        modelBuilder.Entity<AppUser>().Navigation(n => n.Roles).AutoInclude();
        //        

        modelBuilder.Entity<TodoTask>()
            .HasOne(v => v.Owner)
            .WithMany(c => c.Tasks)
            .HasForeignKey(v => v.OwnerId);

        modelBuilder.Entity<AppUserRole>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles);

        modelBuilder.Entity<RefreshToken>().HasKey(u => u.Id);
        modelBuilder.Entity<RefreshToken>().Property(u => u.Id).ValueGeneratedOnAdd();
        modelBuilder.Entity<RefreshToken>().HasOne(u => u.User).WithMany().HasForeignKey(i => i.UserId);

        base.OnModelCreating(modelBuilder);
    }
}
