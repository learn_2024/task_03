﻿using Common.App.Abstractions.Persistance;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Persistence
{
    public class ContextTransaction : IContextTransaction
    {
        private IDbContextTransaction _contextTransaction;

        public ContextTransaction(IDbContextTransaction contextTransaction) 
        {
            _contextTransaction = contextTransaction;
        }
        public async Task CommitAsync(CancellationToken cancellationToken)
        {
           await _contextTransaction.CommitAsync(cancellationToken);
        }

        public void Dispose()
        {
            _contextTransaction.Dispose();
        }

        public ValueTask DisposeAsync()
        {
            return _contextTransaction.DisposeAsync();
            
        }

        public async Task RollbackAsync(CancellationToken cancellationToken)
        {
            await _contextTransaction.RollbackAsync(cancellationToken);
        }
    }
}
