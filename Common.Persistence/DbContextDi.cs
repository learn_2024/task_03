﻿using Common.App.Abstractions.Persistance;
using Common.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Common.Persistence
{
    public static class DbContextDi
    {
        public static IServiceCollection AddApplicationDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DbContext, ApplicationDbContext>(
                options =>
                {
                    options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
                    options.EnableSensitiveDataLogging(true);
                }
            );
            services.AddTransient<IContextTransactionStarter, ContextTransactionStarter>();
            services.AddTransient<IBaseRepository<AppUser>, PostgreSqlBaseRepository<AppUser>>();
            services.AddTransient<IBaseRepository<AppUserRole>, PostgreSqlBaseRepository<AppUserRole>>();
            services.AddTransient<IBaseRepository<TodoTask>, PostgreSqlBaseRepository<TodoTask>>();
            
            return services;
        }
    }
}