﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repo
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, new()
    {
        private readonly List<TEntity> _data = new();
        public TEntity Add(TEntity newTodoItem)
        {
            _data.Add(newTodoItem);
            return newTodoItem;
        }

        public int Count(Expression<Func<TEntity, bool>>? predicate = null)
        {
            IEnumerable<TEntity> records = _data;
            if (predicate == null)
                return records.Count();
            else
                return records.Where(predicate.Compile()).Count();
        }

        public bool Delete(TEntity todoItem)
        {
            return _data.Remove(todoItem);
        }

        public TEntity[] GetList(int? offset = null, int? limit = null, Expression<Func<TEntity, bool>>? predicate = null, Expression<Func<TEntity, object>>? orderBy = null, bool? descending = null)
        {
            IEnumerable<TEntity> result = _data;

            if (predicate != null)
            {
                result = result.Where(predicate.Compile());
            }

            if (orderBy != null)
            {
                result = descending.GetValueOrDefault()
                    ? result.OrderByDescending(orderBy.Compile())
                    : result.OrderBy(orderBy.Compile());
            }

            result = result.Skip(offset.GetValueOrDefault());


            if (limit.HasValue)
            {
                result = result.Take(limit.Value);
            }

            return result.ToArray();
        }

        public TEntity? SingleOrDefault(Expression<Func<TEntity, bool>>? predicate = null)
        {
            if (predicate is null)
            {
                return _data.SingleOrDefault();
            }
            return _data.SingleOrDefault(predicate.Compile());
        }

        public TEntity? Update(TEntity newDataTodoItem, Expression<Func<TEntity, bool>> findById)
        {
            var editedItem = _data.Where(findById.Compile()).FirstOrDefault();

            var myPropertyInfo = typeof(TEntity).GetRuntimeProperties();

            foreach (var property in myPropertyInfo)
            {
                var prop = typeof(TEntity).GetProperty(property.Name);
                if (prop != null)
                {
                    if (prop.CanWrite)
                        prop.SetValue(editedItem, prop.GetValue(newDataTodoItem));
                }
            }

            return editedItem;
        }
    }
}
