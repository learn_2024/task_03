using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Users.App.Query.GetUserList;
using Users.App.Query.GetUserListTotalCount;
using Users.App.Query.GetUserDataById;
using Users.App.Command.AddUser;
using Users.App.Command.EditUser;
using Users.App.Command.ChangePassword;
using Users.App.Command.ResetPassword;
using Users.App.Command.DeleteUser;
using MediatR;

namespace User.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        [AllowAnonymous]
        [HttpGet("users")]
        public async Task<IActionResult> GetUsers([FromQuery] GetUserListQuery query, IMediator mediator, CancellationToken cancellationToken = default)
        {
            //int? offset, string? userName, int? limit
            var list = await mediator.Send(query, cancellationToken);
            Response.Headers.Append("x-Total-Count", (await mediator.Send(new GetUserListTotalCountQuery() { FioUser = query.FioUser }, cancellationToken)).ToString());
            return Ok(list);
        }
        
        [AllowAnonymous]
        [HttpGet]
        [Route("users/{id}")]
        public async Task<IActionResult> UserById(long id, IMediator mediator,  CancellationToken cancellationToken)
        {
            var item = await mediator.Send(new GetUserDataByIdQuery() { Id = id}, cancellationToken);
            return Ok(item);
        }

        [HttpPost]
        [Route("users")]
        public async Task<IActionResult> AddUser([FromBody] AddUserCommand user, IMediator mediator, CancellationToken cancellationToken)
        {
            var item = await mediator.Send(user, cancellationToken);
                //handler.RunAsync(user, cancellationToken);
            return Created($"/users/{item.Id}", item);
        }

        [HttpPut]
        [Route("users")]
        public async Task<IActionResult> UpdateUser([FromBody] EditUserCommand user, IMediator mediator, CancellationToken cancellationToken)
        {
            var item = await mediator.Send(user, cancellationToken);
            return Ok(item);
        }

        [HttpPut]
        [Route("ChangePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangeUserPasswordCommand passData, IMediator mediator, CancellationToken cancellationToken)
        {
            await mediator.Send(passData, cancellationToken);
            return Ok("������ ������� �������");
        }

        [HttpPut]
        [Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand newPassword, IMediator mediator, CancellationToken cancellationToken)
        {
            await mediator.Send(newPassword, cancellationToken);
            return Ok("������ ������� �������");
        }

        [HttpDelete]
        [Route("/users/{userId}")] 
        public async Task<IActionResult> DeleteItem(long userId, IMediator mediator, CancellationToken cancellationToken)
        {
            await mediator.Send(new DeleteUserCommand() { Id = userId}, cancellationToken);
            return Ok();
        }
    }
}